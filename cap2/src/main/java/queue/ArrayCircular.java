package queue;

public class ArrayCircular {
	public static void main(String[] args) {
		final int N = 7;
		int contador = 0;
		
		for(int i = 0; i < N * 3; i++) {			
			contador = (i + 1) % N;
			System.out.println(contador);
		}
	}
}
