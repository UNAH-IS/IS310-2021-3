package queue;

import java.util.AbstractQueue;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Queue;
import array.Factory;

/**
 * An implementation of the Queue<T> interface using an array
 * 
 * All operations takes constant amortized time.
 * 
 * @author morin
 *
 * @param <T>
 */
public class ArrayQueue<T> extends AbstractQueue<T> {
	/**
	 * The class of elements stored in this queue
	 */
	protected Factory<T> factory;

	/**
	 * Array used to store elements
	 */
	protected T[] elements;

	/**
	 * Index of next element to de-queue
	 */
	protected int next;

	/**
	 * Number of elements in the queue
	 */
	protected int size;

	/**
	 * Grow the internal array
	 */
	protected void resize() {
		T[] b = factory.newArray(Math.max(1, size * 2));

		for (int k = 0; k < size; k++) {
			b[k] = elements[(next + k) % elements.length];
		}

		elements = b;
		next = 0;
	}

	/**
	 * Constructor
	 */
	public ArrayQueue(Class<T> t) {
		factory = new Factory<T>(t);
		elements = factory.newArray(1);
		next = 0;
		size = 0;
	}

	/**
	 * Return an iterator for the elements of the queue. This iterator does not
	 * support the remove operation
	 */
	public Iterator<T> iterator() {
		class QueueIterator implements Iterator<T> {
			int k;

			public QueueIterator() {
				k = 0;
			}

			public boolean hasNext() {
				return (k < size);
			}

			public T next() {
				if (k > size) {
					throw new NoSuchElementException();
				}

				T x = elements[(next + k) % elements.length];
				k++;
				return x;
			}

			public void remove() {
				throw new UnsupportedOperationException();
			}
		}
		return new QueueIterator();
	}

	public int size() {
		return size;
	}

	public boolean offer(T x) {
		return add(x);
	}

	public boolean add(T x) {
		if (size + 1 > elements.length) {
			resize();
		}

		elements[(next + size) % elements.length] = x;
		size++;
		return true;
	}

	public T peek() {
		T x = null;

		if (size > 0) {
			x = elements[next];
		}

		return x;
	}

	public T remove() {
		if (size == 0) {
			throw new NoSuchElementException();
		}

		T x = elements[next];
		next = (next + 1) % elements.length;
		size--;

		if (elements.length >= 3 * size) {
			resize();
		}

		return x;
	}

	public T poll() {
		return size == 0 ? null : remove();
	}
	
	public List<T> toCollection() {
		return Arrays.asList(elements);
	}

	public static void main(String args[]) {
		int m = 10000, n = 50;
		Queue<Integer> q = new ArrayQueue<Integer>(Integer.class);

		for (int i = 0; i < m; i++) {
			q.add(i);

			if (q.size() > n) {
				Integer x = q.remove();
				assert (x == i - n);
			}
		}
		Iterator<Integer> i = q.iterator();

		while (i.hasNext()) {
			System.out.println(i.next());
		}
	}
}
