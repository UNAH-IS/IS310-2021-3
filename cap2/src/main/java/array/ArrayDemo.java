package array;

import java.util.ArrayList;

public class ArrayDemo {
	public static void main(String[] args) {
		final int X = 100;
		ArrayList<Integer> a = new ArrayList<Integer>();
		
		for (int i = 0; i < X; i++) {
			long inicio = System.nanoTime();
			a.add(i);
			long duracion = System.nanoTime() - inicio;
			System.out.println("i = " + i + " \ttime = " + duracion);
		}
	}
}
