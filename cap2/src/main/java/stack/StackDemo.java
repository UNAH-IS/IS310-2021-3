package stack;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StackDemo {
	// O(n)
	public static ArrayList<Integer> reverso(ArrayList<Integer> lista) {
		Stack<Integer> s1 = new Stack<Integer>(Integer.class);
		ArrayList<Integer> resultado = new ArrayList<Integer>();
		
		for(int elemento : lista) {
			s1.push(elemento);
		}
		
		Stack<Integer> s2 = new Stack<Integer>(Integer.class);
		while(!s1.isEmpty()) {
			resultado.add(s1.pop());
		}
		
		// Este pide un tipo Collections (un array no lo es)
		return resultado;		
	}
	
	
	public static void main(String[] args) {
		ArrayList<Integer> datos = new ArrayList<Integer>();
		datos.add(4);
		datos.add(8);
		datos.add(16);
		datos.add(32);
		
		ArrayList<Integer> datosReverso = reverso(datos);
		System.out.println(datosReverso);
	}
}
