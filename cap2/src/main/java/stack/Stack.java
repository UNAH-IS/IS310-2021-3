/*
 * LIFO (last in - first out)
 * ultimo entra es el primero sale
 * 
 * Metodos basicos:
 * constructor
 * push -> agrega una nueva cima
 * pop -> devuelve la cima eliminandola
 * top -> devuelve la cima sin eliminarla
 * 
 * Metodos secundarios:
 * clear
 */

package stack;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.EmptyStackException;
import java.util.List;

public class Stack<T> {
	// Es el encargado de contener los elementos del Stack
	private ArrayStack<T> contenedor;
	
	public Stack(Class<T> t) {
		contenedor = new ArrayStack<T>(t);
	}
	
	// Inserta una nueva cima en el Stack
	public void push(T element) {
		contenedor.add(element);
	}
	
	public boolean isEmpty() {
		return contenedor.size() == 0;
	}
	
	private void validateEmptyStack() {
		if (isEmpty()) {
			throw new EmptyStackException();
		}
	}
	
	// Elimina la cima actual y devuelve dicho elemento
	public T pop() {
		validateEmptyStack();		
		return contenedor.remove(contenedor.size() - 1);
	}
	
	// Devuelve la cima actual pero no la elimina
	public T top() {
		validateEmptyStack();
		return contenedor.get(contenedor.size() - 1);
	}
	
	// Limpiar el Stack
	public void clear() {
		contenedor.clear();
	}
	
	@Override
	public String toString() {
		return Arrays.toString(contenedor.toArray());
	}
	
	public List<T> toCollection() {
		return contenedor.toCollection();
	}
	
	public static void main(String[] args) {
		Stack<Integer> stack = new Stack<Integer>(Integer.class);
		
		for (int i = 1; i <= 5; i++) {
			stack.push(i * 10);	
			System.out.println("Top: " + stack.top());
		}

		while (!stack.isEmpty()) {
			System.out.println("Pop: " + stack.pop());
		}		
	}
}
