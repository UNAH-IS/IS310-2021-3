package stack;

import java.util.Arrays;

public class ArrayStackDemo {
	public static void main(String[] args) {
		ArrayStack<Character> stack = new ArrayStack<Character>(Character.class);
		stack.add('B');
		stack.add('R');
		stack.add('E');
		stack.add('D');
		stack.add(2, 'X');
		System.out.println("Visible" + stack);
		System.out.println(Arrays.toString(stack.elements));
		stack.remove(4);
		System.out.println("Visible" + stack);
		System.out.println(Arrays.toString(stack.elements));
		stack.add(3, 'R');
		System.out.println("Visible" + stack);
		System.out.println(Arrays.toString(stack.elements));
	}
}
