package stack;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import array.Factory;

/**
 * This is a copy of the JCF class ArrayList.  It implements the List
 * interface as a single array a.  Elements are stored at positions
 * a[0],...,a[size()-1].  Doubling/halving is used to resize the array
 * a when necessary. 
 * @author morin
 *
 * @param <T> the type of objects stored in the List
 */
public class ArrayStack<T> extends AbstractList<T> {
	/**
	 * keeps track of the class of objects we store
	 */
	Factory<T> factory;
	
	/**
	 * The array used to store elements
	 */
	T[] elements;
	
	/**
	 * The number of elements stored
	 */
	int size;
	
	/**
	 * Resize the internal array
	 */
	protected void resize() {
		// O(n)
		int newSize = size * 2;
		System.out.println("\tResize: " + newSize);
		T[] b = factory.newArray(Math.max(newSize,1));		
		
		// O -> size - 1 = size: n
		for (int i = 0; i < size; i++) {
			b[i] = elements[i];
		}
		
		elements = b;
	}

	/**
	 * Resize the internal array
	 */
	protected void resize(int newSize) {
		T[] b = factory.newArray(newSize);
		System.out.println("\tResize: " + newSize);
		for (int i = 0; i < size; i++) {
			b[i] = elements[i];
		}
		
		elements = b;
	}

	/**
	 * Constructor
	 * @param t0 the type of objects that are stored in this list
	 */
	// O(1)
	public ArrayStack(Class<T> t) {
		factory = new Factory<T>(t);
		elements = factory.newArray(1);
		size = 0;
	}
	
	// O(1)
	private void validateIndex(int i) {
		if (i < 0 || i > size - 1) {
			throw new IndexOutOfBoundsException();
		}
	}

	// O(1)
	public T get(int i) {
		validateIndex(i);		
		return elements[i];
	}
	
	// O(1)
	public int size() {
		return size;
	}
	
	// O(1)
	public T set(int i, T x) {
		validateIndex(i);
		
		T y = elements[i];
		elements[i] = x;
		return y;
	}
	
	// O(n)
	public void add(int i, T x) {
		validateIndex(i);
		
		// O(n) *** amortizado *** 
		if (size + 1 > elements.length) {
			resize();
		}
		
		// size -> i+1: n - i + 1
		for (int j = size; j > i; j--) { 
			elements[j] = elements[j-1];
		}
		
		elements[i] = x;
		size++;
	}

	// The following methods are not strictly necessary. The parent
	// class, AbstractList, has default implementations of them, but
	// our implementations are more efficient - especially addAll
	
	/**
	 * A small optimization for a frequently used method
	 */
	
	// O(1)*
	public boolean add(T x) {
		if (size + 1 > elements.length) {
			resize();
		}
		
		elements[size++] = x;
		return true;
	}
	
	/**
	 * We override addAll because AbstractList implements it by 
	 * repeated calls to add(i,x), which can take time 
	 * O(size()*c.size()).  This happens, for example, when i = 0.
	 * This version takes time O(size() + c.size()).
	 */
	
	public boolean addAll(int i, Collection<? extends T> c) {
		validateIndex(i);
		
		int k = c.size();
		if (size + k > elements.length) { 
			resize(2*(size+k));
		}
		
		for (int j = size+k-1; j >= i+k; j--) {
			elements[j] = elements[j-k];
		}
		
		for (T x : c) {
			elements[i++] = x;
		}
		
		size += k;		
		return true;
	}
	
	// O(n)
	public T remove(int i) {
		validateIndex(i);
		
		T x = elements[i];
		//elements[i] = null;
		for (int j = i; j < size-1; j++) { 
			elements[j] = elements[j+1];
		}
		
		size--;
		
		// O(n) ***amortizado***
		if (elements.length >= 3*size) {
			resize();
		}
		
		return x;
	}	
	
	/**
	 * We override this method because AbstractList implements by
	 * repeated calls to remove(size()), which takes O(size()) time.
	 * This implementation runs in O(1) time.
	 */
	
	// O(1)
	public void clear() {
		size = 0;
		resize();
	}
	
	public List<T> toCollection() {
		return Arrays.asList(elements);
	}
}
