package stack;

import array.Factory;

/**
 * This subclass of ArrayStack overrides some of its functions to make them
 * faster. In particular, it uses System.arraycopy() rather than for loops copy
 * elements between arrays and to shift them within arrays.
 * 
 * @author morin
 *
 * @param <T>
 */
public class FastArrayStack<T> extends ArrayStack<T> {
	public FastArrayStack(Class<T> t0) {
		super(t0);
	}

	protected void resize() {
		T[] b = factory.newArray(Math.max(2 * size, 1));
		System.arraycopy(elements, 0, b, 0, size);
		elements = b;
	}

	protected void resize(int nn) {
		T[] b = factory.newArray(nn);
		System.arraycopy(elements, 0, b, 0, size);
		elements = b;
	}

	public void add(int i, T x) {
		if (i < 0 || i > size) {
			throw new IndexOutOfBoundsException();
		}

		if (size + 1 > elements.length) {
			resize();
		}

		System.arraycopy(elements, i, elements, i + 1, size - i);
		elements[i] = x;
		size++;
	}

	public T remove(int i) {
		if (i < 0 || i > size - 1) {
			throw new IndexOutOfBoundsException();
		}

		T x = elements[i];
		System.arraycopy(elements, i + 1, elements, i, size - i - 1);
		size--;

		if (elements.length >= 3 * size) {
			resize();
		}

		return x;
	}
}
