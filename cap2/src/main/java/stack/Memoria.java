package stack;

public class Memoria {
	static void a() {
		System.out.println("Inicio de funcion A");
		b();
		System.out.println("Fin de funcion A");
	}
	
	static void b() {
		System.out.println("Inicio de funcion B");
		c();
		System.out.println("Fin de funcion B");	
	}
	
	static void c() {
		System.out.println("Inicio de funcion C");
		System.out.println("Fin de funcion C");
	}
	
	public static void main(String[] args) {
		a();
	}
}
