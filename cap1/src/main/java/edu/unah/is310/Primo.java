package edu.unah.is310;

public class Primo {
	public static int[] Instrucciones = {0, 0, 0, 0, 0};

	public static boolean detectarV1(int candidato) {
		// Primo es si hay un numero que divida de forma exacta nuestro candidato
		// y que ese numero no sea ni 1 ni el propio candidato

		Instrucciones[0]++;					// =
		for (int i = 2; i < candidato; i++) {
			if (candidato % i == 0) {
				Instrucciones[0]++;			// return
				return false;
			}
			Instrucciones[0] += 4;			// < // ++ // % // ==
		}


		Instrucciones[0]++;					// return
		return true;
	}

	public static boolean detectarV2(int candidato) {
		Instrucciones[1]++;					// =
		for (int i = 2; i < candidato / 2; i++) {
			Instrucciones[1] += 3; 			// < // / // ++
			if (candidato % 2 != 0) {
				if (i % 2 == 0) {
					Instrucciones[1]++;		// ++
					i++;
				}
			}
			if (candidato % 3 != 0) {
				if (i % 3 == 0) {
					Instrucciones[1]++;		// ++
					i++;
				}
			}
			if (candidato % 5 != 0) {
				if (i % 5 == 0) {
					Instrucciones[1]++;		// ++
					i++;
				}
			}
			if (candidato % 7 != 0) {
				if (i % 7 == 0) {
					Instrucciones[1]++;		// ++
					i++;
				}
			}
			if (candidato % 11 != 0) {
				if (i % 11 == 0) {
					Instrucciones[1]++;		// ++
					i++;
				}
			}
			if (candidato % 13 != 0) {
				if (i % 13 == 0) {
					Instrucciones[1]++;		// ++
					i++;
				}
			}
			if (candidato % 17 != 0) {
				if (i % 17 == 0) {
					Instrucciones[1]++;		// ++
					i++;
				}
			}
			if (candidato % 19 != 0) {
				if (i % 19 == 0) {
					Instrucciones[1]++;		// ++
					i++;
				}
			}
			if (candidato % 23 != 0) {
				if (i % 23 == 0) {
					Instrucciones[1]++;		// ++
					i++;
				}
			}
			if (candidato % i == 0) {
				Instrucciones[1]++;			// return 
				return false;
			}
			Instrucciones[1] += 4;			// % // == // % // ==
		}
		
		Instrucciones[1]++;					// return
		return true;
	}

	public static boolean detectarV3(int candidato) {
		if (candidato == 2) {
			Instrucciones[2]++;
			return true;
		}
		
		if (candidato % 2 == 0) {
			Instrucciones[2]++;
			return false;
		}

		Instrucciones[2] += 4;			// == // % // == // =
		for (int i = 3; i < candidato / 3; i += 2) {
			Instrucciones[2] += 6;		// < // / // = // + // % // ==
			if (candidato % i == 0) {
				Instrucciones[2]++;		// return
				return false;
			}
		}

		Instrucciones[2]++; 			// return
		return true;
	}

	public static boolean detectarV4(int candidato) {
		if (candidato == 2) {
			Instrucciones[3]++;
			return true;
		}
		
		if (candidato % 2 == 0) {
			Instrucciones[3]++;
			return false;
		}

		int valorMax = (int) Math.floor(Math.sqrt(candidato));
		Instrucciones[3] += 8; 			// == // % // == // valorMax // (int) // floor // sqrt // =
		for (int i = 3; i < valorMax; i += 2) {
			Instrucciones[3] += 5;		// < // = // + // % // ==
			if (candidato % i == 0) {
				Instrucciones[3]++;
				return false;
			}
		}

		Instrucciones[3]++;
		return true;
	}

	public static boolean detectarV5(int candidato) {
		Instrucciones[4]++;				// =
		for (int i = 2; i < Math.sqrt(candidato); i++) {
			Instrucciones[4] += 5; 		// < // Math.sqrt // ++ // % // ==
			if (candidato % i == 0) {
				Instrucciones[4]++;
				return false;
			}
		}

		Instrucciones[4]++;
		return true;

	}

	public static void main(String[] args) {
		final int N = 100000073;
		long[] inicio = new long[5];
		long[] duracion = new long[5];

		System.out.println("Version 1");
		inicio[0] = System.currentTimeMillis();
		System.out.println(detectarV1(N));
		System.out.println(Primo.Instrucciones[0]);
		duracion[0] = System.currentTimeMillis() - inicio[0];
		System.out.println(duracion[0]);
		System.out.println("---------------------");

		System.out.println("Version 2");
		inicio[1] = System.currentTimeMillis();
		System.out.println(detectarV2(N));
		System.out.println(Primo.Instrucciones[1]);
		duracion[1] = System.currentTimeMillis() - inicio[1];
		System.out.println(duracion[1]);
		System.out.println("---------------------");

		System.out.println("Version 3");
		inicio[2] = System.currentTimeMillis();
		System.out.println(detectarV3(N));
		System.out.println(Primo.Instrucciones[2]);
		duracion[2] = System.currentTimeMillis() - inicio[2];
		System.out.println(duracion[2]);
		System.out.println("---------------------");

		System.out.println("Version 4");
		inicio[3] = System.currentTimeMillis();
		System.out.println(detectarV4(N));
		System.out.println(Primo.Instrucciones[3]);
		duracion[3] = System.currentTimeMillis() - inicio[3];
		System.out.println(duracion[3]);
		System.out.println("---------------------");
		
		System.out.println("Version 5");
		inicio[4] = System.currentTimeMillis();
		System.out.println(detectarV5(N));
		System.out.println(Primo.Instrucciones[4]);
		duracion[4] = System.currentTimeMillis() - inicio[4];
		System.out.println(duracion[4]);

	}
}
