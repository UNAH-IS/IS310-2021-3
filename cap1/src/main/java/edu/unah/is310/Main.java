package edu.unah.is310;

public class Main {
	public static void main(String[] args) {
		final int N = 100;
		long inicio1, inicio2, inicio3, inicio4;
		long duracion1, duracion2, duracion3, duracion4;
		
		System.out.println("Para un N = " + N);
		
		System.out.print("For sencillo... ");		
		inicio1 = System.currentTimeMillis();		
		// inicia evaluacion
		for (int i = 0; i < N; i++);
		// termina evaluacion		
		duracion1 = System.currentTimeMillis() - inicio1;
		System.out.println(duracion1 + " ms");
		
		System.out.print("For doble... ");		
		inicio2 = System.currentTimeMillis();		
		// inicia evaluacion
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++);
		// termina evaluacion		
		duracion2 = System.currentTimeMillis() - inicio2;
		System.out.println(duracion2 + " ms");
		
		System.out.print("For triple... ");		
		inicio3 = System.currentTimeMillis();		
		// inicia evaluacion
		for (int i = 0; i < N; i++)
			for (int j = 0; j < N; j++)
				for (int k = 0; k < N; k++);
		// termina evaluacion		
		duracion3 = System.currentTimeMillis() - inicio3;
		System.out.println(duracion3 + " ms");
		
		System.out.print("For doble (sin anidar)... ");		
		inicio4 = System.currentTimeMillis();		
		// inicia evaluacion
		for (int i = 0; i < N; i++);
		for (int j = 0; j < N; j++);
		// termina evaluacion		
		duracion4 = System.currentTimeMillis() - inicio4;
		System.out.println(duracion4 + " ms");
	}
}
