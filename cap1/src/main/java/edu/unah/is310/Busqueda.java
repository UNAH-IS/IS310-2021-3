package edu.unah.is310;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Busqueda {
	public static int instrucciones;

	public static int[] crearArrayOrdenado(int limite) {
		int[] valores = new int[limite];
		for (int i = 0; i < limite; i++) {
			valores[i] = i;
		}
		return valores;
	}
	
	public static int[] crearArrayRandomOrdenado(int limite) {
		/*
		 * f(n) ~ 3n
		 * O(f(n)) = O(n)
		 */
		boolean[] universo = new boolean[limite];
		Random random = new Random();
		ArrayList<Integer> contenedor = new ArrayList();
		
		for (int i = 0; i < limite; i++) {
			int posicion = random.nextInt(limite);
			universo[posicion] = true;
		}
		
		
		// Llenar valores
		for (int i = 0; i < universo.length; i++) {
			if (universo[i]) {
				contenedor.add(i);
			}
		}
		
		// Completar el resultado (existen varias formas pero encontre esta opcion la mas simple)
		int[] respuesta = new int[contenedor.size()];
		for (int i = 0; i < contenedor.size(); i++) {
			respuesta[i] = contenedor.get(i);
		}
		
		return respuesta;
	}

	public static int busquedaLineal(int arr[], int valor) {
		// O(n)
		instrucciones = 0;
		for (int i = 0; i < arr.length; i++) {
			instrucciones++; // instrucciones del for (simplificado)
			if (arr[i] == valor) {
				return i;
			}
		}
		return -1;
	}

	public static int busquedaBinaria(int arr[], int valor) {
		// O(log n)
		instrucciones = 0;
		int izquierda = 0;
		int derecha = arr.length - 1;

		while (izquierda <= derecha) {
			instrucciones++;
			int mitad = izquierda + (derecha - izquierda) / 2;

			if (arr[mitad] == valor) {
				return mitad;
			}

			if (arr[mitad] < valor) {
				izquierda = mitad + 1;
			} else {
				derecha = mitad - 1;
			}
		}

		return -1;
	}

	public static void main(String[] args) {
		Random random = new Random();
		final int DIMENSION_ARRAY = 7;
		final int X = 1; //random.nextInt(DIMENSION_ARRAY + 10);
		int[] elementos = {4, 2, 8, 3, 2, 1, 9, 7};//crearArrayOrdenado(DIMENSION_ARRAY);
		
		System.out.print("Arreglo original: ");
		System.out.println(Arrays.toString(elementos));
		
		System.out.println("\nEl valor a encontrar: " + X);

		int posicionLineal = Busqueda.busquedaLineal(elementos, X);
		System.out.println("\nLineal");
		System.out.printf("Posicion: %d %nInstrucciones: %d%n", posicionLineal, instrucciones);
	
		int posicionBinaria = Busqueda.busquedaBinaria(elementos, X);
		System.out.println("\nBinario");		
		System.out.printf("Posicion: %d %nInstrucciones: %d%n", posicionBinaria, instrucciones);

	}
}
