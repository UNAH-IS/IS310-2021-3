package edu.unah.is310;

public class Crecimiento {
	static int instrucciones;
	
	public static void constante(int n) {
		int valor = n * n;	
		// En este caso solo serian 2 instrucciones. No importa el valor de 'n'		
		instrucciones = 2;
		System.out.printf("O(1) -> n=%d  instrucciones=%d%n", n, instrucciones);
	}
	
	public static void constante2(int n) {
		// Parece lineal pero al fijarnos en este bucle nos damos cuenta que no
		instrucciones = 0;
		for (int i = 0; i < 3; i++) {
			instrucciones++;
		}
		System.out.printf("O(1) -> n=%d  instrucciones=%d%n", n, instrucciones);
	}
	
	public static void lineal(int n) {
		instrucciones = 0;
		for (int i = 0; i < n; i++) {
			instrucciones++;
		}
		System.out.printf("O(n) -> n=%d  instrucciones=%d%n", n, instrucciones);
	}
	
	public static void lineal2(int n) {
		// Siempre es lineal pero el coeficiente es menor que el anterior
		instrucciones = 0;
		for (int i = 0; i < n; i += 2) {
			instrucciones++;
		}
		System.out.printf("O(n) -> n=%d  instrucciones=%d%n", n, instrucciones);
	}
	
	public static void lineal3(int n) {
		// Tambien es lineal pero el coeficiente es mayor que los dos anteriores
		instrucciones = 0;
		for (int i = 0; i < n; i++) {
			instrucciones++;
		}
		for (int j = 0; j < n; j++) {
			instrucciones++;
		}
		System.out.printf("O(n) -> n=%d  instrucciones=%d%n", n, instrucciones);
	}
	
	public static void logartmica(int n) {
		// Crece pero no en la misma proporcion que los algoritmos lineales
		instrucciones = 0;
		for (int i = 1; i <= n; i *= 2) {
			instrucciones++;
		}
		System.out.printf("O(log n) -> n=%d  instrucciones=%d%n", n, instrucciones);
	}	
	
	public static void main(String[] args) {
		final int N = 100000000;
		constante(N);
		constante2(N);
		lineal(N);
		lineal2(N);
		lineal3(N);
		logartmica(N);
	}
}
