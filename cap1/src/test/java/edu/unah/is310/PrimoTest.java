package edu.unah.is310;

import static org.junit.Assert.*;

import org.junit.Test;

public class PrimoTest {

	@Test
	public void testVersion1() {
		assertEquals(true, Primo.detectarV1(11));
		assertEquals(true, Primo.detectarV1(101));
		assertEquals(false, Primo.detectarV1(102));
		assertEquals(false, Primo.detectarV1(111));
	}
	
	@Test
	public void testVersion2() {
		assertEquals(true, Primo.detectarV2(11));
		assertEquals(true, Primo.detectarV2(101));
		assertEquals(false, Primo.detectarV2(102));
		assertEquals(false, Primo.detectarV2(111));
	}

}
