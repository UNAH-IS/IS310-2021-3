package ods;

import java.util.Queue;

public class SLLDemo {
	public static void main(String[] args) {
		Queue<Integer> q = new SLList<Integer>();

		for (int i = 0; i < 100; i++) {
			q.add(i);
		}

		System.out.println(q);

		for (int i = 0; i < 50; i++) {
			q.remove();
		}

		System.out.println(q);

		for (int i = 100; i < 200; i++) {
			q.add(i);
		}

		System.out.println(q);

		for (int i = 0; i < 50; i++) {
			q.remove();
		}

		System.out.println(q);

		while (!q.isEmpty()) {
			q.remove();
		}

		System.out.println(q);
	}
}
