package ods;

import java.util.ListIterator;

public class DLLDemo {
	public static void main(String[] args) {
		DLList<Character> lista = new DLList<Character>();
		
		lista.add('A');
		System.out.println(lista);
		lista.add('B');		
		System.out.println(lista);
		lista.add(0, 'C');
		System.out.println(lista);
		
		System.out.println("Recorrido hacia adelante");
		ListIterator<Character> iterator = lista.listIterator(0);
		
		while(iterator.hasNext()) {
			System.out.println(iterator.next());
		}
		
		System.out.println("Recorrido al reves");
		iterator = lista.listIterator(lista.size() - 1);		
		
		while(iterator.hasPrevious()) {
			System.out.println(iterator.previous());
		}
	}
}
