package intro;

public class Nodo {
	private int dato;
	private Nodo siguiente;
	
	public Nodo(int x) {
		this.dato = x;
		this.siguiente = null;
	}
	
	public void setSiguiente(Nodo n) {
		this.siguiente = n;
	}
	
	public Nodo getSiguiente() {
		return this.siguiente;
	}
	
	public int getDato() {
		return this.dato;
	}
	
	public void setDato(int x) {
		this.dato = x;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.valueOf(this.dato);
	}
	
	public static void main(String[] args) {
		Nodo n1 = new Nodo(10);
		System.out.println(n1.hashCode());
		
		// Esta es un referencia completa (nodo a nodo) son el mismo objeto
		// n2->n1
		Nodo n2 = n1;
		System.out.println(n2.hashCode());
		
		Nodo n3 = new Nodo(30);
		
		// n3.siguiente->n2
		n3.setSiguiente(n2);
		System.out.println(n2.getDato());
		System.out.println(n3.getDato());
		
		// n3.getSiguiente() es un nodo por si mismo distinto de n3.
		System.out.println(n3.getSiguiente().getDato());
		n3.getSiguiente().setDato(200);
		System.out.println(n1);
		System.out.println(n2);
		System.out.println(n3);
		
	}
}
