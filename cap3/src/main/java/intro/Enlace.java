package intro;

import java.awt.Point;

public class Enlace {
	public static void main(String[] args) {
		Point punto1 = new Point(3, 4);
		
		
		// Esto es una referencia
		Point punto2 = punto1;
		
		
		System.out.println(punto1);
		punto2.x = 454;
		System.out.println("Aqui se modifico punto2 x=" + punto2.x);
		System.out.println(punto1);
	}
}
