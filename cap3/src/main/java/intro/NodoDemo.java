package intro;

public class NodoDemo {
	public static void main(String[] args) {
		Nodo[] nodos = new Nodo[10];
		
		for (int i = 0; i < nodos.length; i++) {
			nodos[i] = new Nodo(i*10);
		}
		
		for (int i = 0; i < nodos.length - 1; i++) {
			nodos[i].setSiguiente(nodos[i+1]);
		}
		
		// 0
		System.out.println(nodos[0]);
		// 10
		System.out.println(nodos[0].getSiguiente());
		// 20
		System.out.println(nodos[0].getSiguiente().getSiguiente());
		// 30
		System.out.println(nodos[0].getSiguiente().getSiguiente().getSiguiente());
		
		nodos[5].setSiguiente(null);
		
		System.out.println("Inicia el bucle");
		
		// Inicializacion
		Nodo temp = nodos[0];
		while (temp != null) {
			System.out.println(temp);
			temp = temp.getSiguiente();
		}
	}
}
