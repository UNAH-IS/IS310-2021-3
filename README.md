# IS310 2021 3

Código de Algoritmos y Estructura de Datos para el III PAC 2021.

## Proyectos
- [Eficiencia](https://gitlab.com/UNAH-IS/IS310-2021-3/-/tree/main/eficiencia)

## Instalación
Solo es necesario clonar el proyecto. Para esto se realiza lo siguiente:
- Instalar GIT segun los [siguientes pasos](https://github.com/git-guides/install-git)
- Ejecutar el comando: ```git clone https://gitlab.com/UNAH-IS/IS310-2021-3.git```

Cuando se desee actualizar el repositorio debe ejecutar el siguiente comando:
``` git pull origin main```

## Licencia
Todo el proyecto es Creative Commons.

